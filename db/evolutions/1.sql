# Add cities

# --- !Ups

INSERT INTO m_city(id, name, lat, lon) VALUES (1, 'Кемерово', 0, 0);
INSERT INTO m_city(id, name, lat, lon) VALUES (2, 'Новокузнецк', 0, 0);
INSERT INTO m_city(id, name, lat, lon) VALUES (3, 'Прокопьевск',0, 0);
INSERT INTO m_city(id, name, lat, lon) VALUES (4, 'Таштагол',0, 0);
INSERT INTO m_city(id, name, lat, lon) VALUES (5, 'Киселевск',0, 0);
INSERT INTO m_city(id, name, lat, lon) VALUES (6, 'Белово',0, 0);
INSERT INTO m_city(id, name, lat, lon) VALUES (7, 'Ленинск-Кузнецкий',0, 0);
INSERT INTO m_city(id, name, lat, lon) VALUES (8, 'Междуреченск',0, 0);

INSERT INTO m_city(id, name, lat, lon) VALUES (9, 'Березовский',0, 0);
INSERT INTO m_city(id, name, lat, lon) VALUES (10, 'Калтан',0, 0);
INSERT INTO m_city(id, name, lat, lon) VALUES (11, 'Полысаево',0, 0);
INSERT INTO m_city(id, name, lat, lon) VALUES (12, 'Анджеро-Судженск',0, 0);
INSERT INTO m_city(id, name, lat, lon) VALUES (13, 'Гурьевск',0, 0);
INSERT INTO m_city(id, name, lat, lon) VALUES (14, 'Мариинск',0, 0);


INSERT INTO m_city(id, name, lat, lon) VALUES (15, 'Мыски',0, 0);
INSERT INTO m_city(id, name, lat, lon) VALUES (16, 'Осинники',0, 0);
INSERT INTO m_city(id, name, lat, lon) VALUES (17, 'Топки',0, 0);
INSERT INTO m_city(id, name, lat, lon) VALUES (18, 'Тайга',0, 0);
INSERT INTO m_city(id, name, lat, lon) VALUES (19, 'Юрга',0, 0);
INSERT INTO m_city(id, name, lat, lon) VALUES (20, 'Салаир',0, 0);

