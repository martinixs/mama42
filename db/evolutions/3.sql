# Add main category

# --- !Ups

INSERT INTO m_secret_question(id, question) VALUES (1, 'Какое прозвище было у вас в детстве?');
INSERT INTO m_secret_question(id, question) VALUES (2, 'Какое отчество вашего самого старого родственника?');
INSERT INTO m_secret_question(id, question) VALUES (3, 'Как звали вашу первую плюшевую игрушку?');
INSERT INTO m_secret_question(id, question) VALUES (4, 'В каком месте встретились ваши родители?');
INSERT INTO m_secret_question(id, question) VALUES (5, 'Какая девичья фамилия вашей бабушки?');
INSERT INTO m_secret_question(id, question) VALUES (6, 'Где вы были когда услышали о трагедии девятого сентября?');
