
var mamaApp = angular.module('mamaApp', ['summernote']);

mamaApp.controller('ArticleController', function($scope, $http) {
    $scope.newArticle = function() {
        var article = {
            articleHeader : $scope.articleHeader,
            articleBody: $scope.articleBody,
            articleIsPub: $scope.articleIsPub,
            articleCategory: $scope.articleCategory,
            articleShot: $scope.articleShot
        }
        $http.post('/admin/dashboard/articles/new', article);
    }

});
