package models;

import play.db.jpa.Model;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "m_category")
public class Category extends Model {
    public Long parent;
    public String name;

    public Category() {
    }

    public Category(Long parent, String name) {
        this.parent = parent;
        this.name = name;
    }
}
