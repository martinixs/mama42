package models;

/**
 * Created by marina on 20.09.2014.
 */
public enum Gender {
    MALE, FEMALE
}
