package models;

import play.db.jpa.Model;

import javax.persistence.*;

@Entity
@Table(name = "m_image")
public class Image extends Model {
    public String description;
    @OneToOne(fetch = FetchType.LAZY)
    public User user;
    public String path;
    @Column(nullable = true)
    public String source;

    public Image() {
    }
}
