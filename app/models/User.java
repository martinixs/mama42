package models;

import com.lambdaworks.crypto.SCryptUtil;
import org.hibernate.annotations.*;
import org.hibernate.annotations.CascadeType;
import org.joda.time.DateTime;
import play.data.validation.Email;
import play.data.validation.Required;
import play.data.validation.Unique;
import play.db.jpa.Model;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;

import java.util.List;

import static java.lang.Math.round;
import static java.lang.Math.random;
import static java.lang.Math.pow;
import static java.lang.Math.abs;
import static java.lang.Math.min;
import static org.apache.commons.lang.StringUtils.leftPad;

@Entity
@Table(name = "m_users")
public class User extends Model {
    @Column(name = "nick", unique = true)
    @Required(message = "Это поле обязательно для заполнения")
    @Unique(message = "Пользователь с таким ником уже существует")
    public String nick;

    @Column(name = "email", unique = true)
    @Email(message = "Невалидный адрес электронной почты")
    @Required(message = "Это поле обязательно для заполнения")
    @Unique(message = "Пользователь с таким адресом электронной почты уже существует")
    public String email;

    public String gender;

    @Column(name = "date_of_birth", nullable = true)
    public DateTime dateOfBirth;

    @Column(name = "pass_hash")
    public String passwordHash;

    @Column(name = "pass", nullable = true)
    @Transient
    @Required(message = "Это поле обязательно для заполнения")
    public String password;

    @Column(name = "active_user")
    public Boolean activeUser;

    @Column(name = "role")
    public String role;

    @Column(name = "verifyCode", nullable = true)
    public String verifyCode;

    @Column(name = "date_created")
    public DateTime dateCreated;

    @Column(name = "date_updated")
    public DateTime dateUpdated;

    @Column(name = "date_last_login")
    public DateTime dateLastLogin;


    @Column(name = "secret_question", nullable = true)
    public SecretQuestion secretQuestion;

    @Column(name = "secret_answer", nullable = true)
    public String secretAnswer;


    @OneToOne(fetch = FetchType.LAZY)
    public City city;

    @ManyToMany
    public List<Child> childrens;


    public User() {}

    public User(String nick, String email, String password, Boolean activeUser, String role) {
        this.nick = nick;
        this.email = email;
        this.activeUser = activeUser;
        this.role = role;
        setPassword(password);
        this.verifyCode = gen(40);
        this.dateCreated = new DateTime();
    }

    public User(String nick, String email, String password) {
        this.nick = nick;
        this.email = email;
        this.activeUser = false;
        this.role = Role.USER.name();
        this.verifyCode = gen(40);
        setPassword(password);

    }

    public void setPassword(String password) {
        this.password = password;
        this.passwordHash = SCryptUtil.scrypt(password, 16384, 8, 1);
    }

    public static boolean isValidLogin(String nick, String password) {
        User user = User.find("byNick", nick).first();
        return user != null && SCryptUtil.check(password, user.passwordHash);
    }

    public static String gen(int length) {
        StringBuffer sb = new StringBuffer();
        for (int i = length; i > 0; i -= 12) {
            int n = min(12, abs(i));
            sb.append(leftPad(Long.toString(round(random() * pow(36, n)), 36), n, '0'));
        }
        return sb.toString();
    }

    public void setCity(City city) {
        this.city = city;
    }

    public String getSecretAnswer() {
        return secretAnswer;
    }

    public void setSecretAnswer(String secretAnswer) {
        this.secretAnswer = secretAnswer;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public DateTime getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(DateTime dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }

    public String getPassword() {
        return password;
    }

    public Boolean getActiveUser() {
        return activeUser;
    }

    public void setActiveUser(Boolean activeUser) {
        this.activeUser = activeUser;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getVerifyCode() {
        return verifyCode;
    }

    public void setVerifyCode(String verifyCode) {
        this.verifyCode = verifyCode;
    }

    public DateTime getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(DateTime dateCreated) {
        this.dateCreated = dateCreated;
    }

    public DateTime getDateUpdated() {
        return dateUpdated;
    }

    public void setDateUpdated(DateTime dateUpdated) {
        this.dateUpdated = dateUpdated;
    }

    public DateTime getDateLastLogin() {
        return dateLastLogin;
    }

    public void setDateLastLogin(DateTime dateLastLogin) {
        this.dateLastLogin = dateLastLogin;
    }

    public SecretQuestion getSecretQuestion() {
        return secretQuestion;
    }

    public void setSecretQuestion(SecretQuestion secretQuestion) {
        this.secretQuestion = secretQuestion;
    }

    public City getCity() {
        return city;
    }

    public List<Child> getChildrens() {
        return childrens;
    }

    public void setChildrens(List<Child> childrens) {
        this.childrens = childrens;
    }
}
