package models;

/**
 * Created by marina on 30.11.14.
 */
public enum Section {
    ARTICLE, POSTER, PLACE, SERVICES
}
