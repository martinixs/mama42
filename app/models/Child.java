package models;

import org.joda.time.DateTime;
import play.db.jpa.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "m_child")
public class Child extends Model {
    public String name;
    public Gender gender;

    @Column(name = "date_of_birth")
    public DateTime dateOfBirth;

    public Child() {
    }
}
