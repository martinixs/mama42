package models;

import org.joda.time.DateTime;
import play.db.jpa.Model;

import javax.persistence.*;

@Entity
@Table(name = "m_article")
public class Article extends Model {

    public String header;
    public byte[] body;

    public String shotTitle;

    @ManyToOne(fetch = FetchType.LAZY)
    public Image image;

    @OneToOne(fetch = FetchType.LAZY)
    public Category category;

    @OneToOne(fetch = FetchType.LAZY)
    public User user;

    @Column(name = "date_created")
    public DateTime dateCreated;
    @Column(name = "date_published", nullable = true)
    public DateTime datePublished;

    @Column(name = "is_published")
    public Boolean isPublished;

    public Article() {
    }



}
