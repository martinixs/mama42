package models;

import play.db.jpa.Model;

import javax.persistence.Entity;
import javax.persistence.Table;


@Entity
@Table(name = "m_city")
public class City extends Model {
    public String name;
    public Double lon;
    public Double lat;

    public City(String name, Double lon, Double lat) {
        this.name = name;
        this.lon = lon;
        this.lat = lat;
    }

}
