package controllers;

import models.City;
import models.Role;
import models.User;
import play.data.validation.Valid;
import play.data.validation.Validation;
import play.mvc.Controller;

import java.util.List;

public class Auth extends Controller {

    public static void login() {
        List<City> cities = City.findAll();
        render(cities);
    }

    public static void doSignIn(User user) {
        if (!User.isValidLogin(user.nick, user.password)) {
            params.flash();
            Validation.addError("user.nick", "Неправильный логин или пароль" );
            Validation.keep();
            login();
        }

        User u = User.find("byNick", user.nick).first();
        if(u != null && !u.activeUser) {
            params.flash();
            Validation.addError("user.nick", "Ваш аккаунт не активирован. Письмо с кодом активации должно прийти на указанную вами электронную почту");
            Validation.keep();
            login();
        }

        session.put("user", u.nick);
        session.put("role", u.role);
        Application.index();

    }

    public static void doSignUp(@Valid User user, Long city) {
        City c = City.findById(city);
        user.setCity(c);
        user.role = Role.USER.name();
        user.activeUser = new Boolean(false);
        user.verifyCode = User.gen(42);
        if (!user.validateAndSave()) {
            params.flash();
            Validation.keep();
            signUp();
        }

        EmailsController.confirmationMail(user.email, user.verifyCode);
        Application.index();
    }
    public static void signUp() {
        List<City> cities = City.findAll();
        render(cities);
    }

    public static void logout() {
        session.remove("user");
        session.remove("role");
        Application.index();
    }

    public static void confirmMail(String code) {
        User user = User.find("byVerifyCode", code).first();
        if(user != null) {
            user.setVerifyCode(null);
            user.setActiveUser(Boolean.TRUE);
            user._save();
        }

        Application.index();
    }

    public static User getLoggenedInUser() {
        return User.find("byNick", session.get("user")).first();
    }
}
