package controllers.admin;


import com.google.gson.*;

import controllers.Application;
import controllers.Auth;
import models.Category;

import models.Role;
import models.Section;
import models.User;
import models.Image;

import org.apache.commons.codec.binary.Base64;

import org.joda.time.DateTime;
import play.Play;
import play.mvc.Controller;

import java.io.*;
import java.util.Date;
import java.util.List;


public class Article extends Controller {
    public static List<Category> listArticleSection() {
        Long categoryId = ((Category) Category.find("byName", Section.ARTICLE.name()).first()).getId();
        return Category.find("byParent", categoryId).fetch();
    }
    public static void articles() {
        List<models.Article> articles = models.Article.find("byIsPublished", true).fetch();
        System.out.println("Size: " + articles.size());
        List<Category> categories = listArticleSection();
        render(articles, categories);
    }

    public static void adminArticles() {

        User user = Auth.getLoggenedInUser();
        if(user.role.equals(Role.ADMIN.name())) {
            render();
        } else {
            forbidden();
        }
    }

    public static void article(String id) throws UnsupportedEncodingException {
        models.Article a = models.Article.find("byId", Long.parseLong(id)).first();
        String body = new String(a.body, "UTF-8");
        List<Category> categories = listArticleSection();
        render(a, body, categories);
    }

    public static void newArticle() {
        User user = Auth.getLoggenedInUser();
        if(user != null ) {
            if(user.role.equals(Role.ADMIN.name())) {
                List<Category> categories = listArticleSection();
                render(categories);
            } else {
                forbidden();
            }
        } else {
            Application.index();
        }
    }

    public static void doNewArticle() {
        models.Article article = new models.Article();
        article.user = Auth.getLoggenedInUser();



        JsonObject jobject = (JsonObject)new JsonParser().parse(params.get("body"));

        Image img = new Image();
        img.source = jobject.getAsJsonPrimitive("photoSource").getAsString();
        img.user =  article.user;
        img.description = jobject.getAsJsonPrimitive("photoName").getAsString();



        String compliteImageData = jobject.getAsJsonPrimitive("photo").getAsString();


        String imageDataBytes = compliteImageData.substring(compliteImageData.indexOf(",") + 1);

        //System.out.println("-----------------------img" + imageDataBytes);
        try {
            String path =  + new Date().getTime() + ".jpg";
            img.path = path;

            File f = new File(Play.getFile("").getAbsolutePath() + File.separator + "public" + File.separator + "uploads" + File.separator + path);

            Base64 decoder = new Base64();
            byte[] imgBytes = decoder.decode(imageDataBytes);

            FileOutputStream osf = new FileOutputStream(f);
            osf.write(imgBytes);
            osf.flush();
            osf.close();

        } catch (Exception ex) {

        }

        img._save();
        article.header = jobject.getAsJsonPrimitive("articleHeader").getAsString();
        String body = jobject.getAsJsonPrimitive("articleBody").getAsString();
        article.body = body.getBytes();
        article.shotTitle = jobject.getAsJsonPrimitive("articleShot").getAsString();

        Long categoryId = Long.getLong(jobject.getAsJsonPrimitive("articleCategory").getAsString());
        article.category =  Category.find("byId", categoryId).first();

        article.isPublished = jobject.getAsJsonPrimitive("articleIsPub").getAsBoolean();
        article.dateCreated = new DateTime();

        if(article.isPublished) {
            article.datePublished = article.dateCreated;
        }

        article.image = img;

        article.save();



        //newArticle();
    }

    public static void uploadImage() {

    }


}
