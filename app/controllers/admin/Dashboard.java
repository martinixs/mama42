package controllers.admin;

import controllers.Auth;
import models.Role;
import models.User;
import play.mvc.Controller;


public class Dashboard extends Controller {
    public static void index() {
        User u = Auth.getLoggenedInUser();
        if(u != null) {
            if(u.role.equals(Role.ADMIN.name()))
                render();
            else
                forbidden();
        } else {
            error();
        }
    }
}
