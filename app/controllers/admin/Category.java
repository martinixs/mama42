package controllers.admin;

import controllers.Application;
import controllers.Auth;
import models.Role;
import models.Section;
import models.User;
import play.mvc.Controller;

import java.util.List;


public class Category extends Controller {
    public static List<Category> listParentCategory() {
        return models.Category.find("byParent", new Long(0)).fetch();
    }
    public static void newCategory() {
        User user = Auth.getLoggenedInUser();
        if(user != null ) {
            if(user.role.equals(Role.ADMIN.name())) {
                List<Category> categories = listParentCategory();
                render(categories);
            } else {
                forbidden();
            }
        } else {
            Application.index();
        }
    }

    public static void doNewCategory(models.Category category) {
        category.save();
        newCategory();
    }
}
