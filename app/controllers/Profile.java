package controllers;

import models.User;
import play.mvc.Controller;

public class Profile extends Controller{
    public static void profile() {
        User user = Auth.getLoggenedInUser();
        if(user != null) {
            render(user);
        } else {
            forbidden("Пожалуйста авторизируйтесь");
        }
    }
}
