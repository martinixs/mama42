package controllers;

import play.Play;
import play.mvc.Http;
import play.mvc.Mailer;

public class EmailsController extends Mailer {

    public static String APPLICATION_URL = Http.Request.current() == null ? Play.configuration.getProperty("application.baseUrl", "application.baseUrl") : Http.Request.current().getBase();

    public static void confirmationMail(String email, String code) {
        String applicationUrl = APPLICATION_URL;
        setContentType("text/html; charset=UTF-8");
        setSubject("Verification you email");
        addRecipient(email);
        setFrom("Mama42 <kajgorodovamarina@gmail.com>");
        send(code, applicationUrl);
    }
}
