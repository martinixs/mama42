package controllers;


import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import models.Category;
import models.Image;
import models.Section;
import models.User;
import org.apache.commons.codec.binary.Base64;
import org.joda.time.DateTime;
import play.Play;
import play.mvc.Controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.List;

public class Article extends Controller {

    public static List<Category> listArticleSection() {
        Long categoryId = ((Category) Category.find("byName", Section.ARTICLE.name()).first()).getId();
        System.out.println("____________" + categoryId);
        return Category.find("byParent", categoryId).fetch();
    }


    public static void articles() {
        List<models.Article> articles = models.Article.find("byIsPublished", true).fetch();
        System.out.println("Size: " + articles.size());
        List<Category> categories = listArticleSection();
        render(articles, categories);
    }



    public static void article(String id) throws UnsupportedEncodingException {
        models.Article a = models.Article.find("byId", Long.parseLong(id)).first();
        String body = new String(a.body, "UTF-8");
        List<Category> categories = listArticleSection();
        render(a, body, categories);
    }

}
