package ext;

import org.joda.time.DateTime;
import play.templates.JavaExtensions;


public class DateTimeExtensions extends JavaExtensions {

    public static String format(DateTime datetime, String format) {
        return datetime==null ? "" : datetime.toString(format);
    }
}
